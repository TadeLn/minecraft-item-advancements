data = [];

tbody = document.querySelector("#mw-content-text > div > div:nth-child(24) > table > tbody");

for (var i = 0; i < tbody.childElementCount; i++) {
    if (tbody.children[i].children[0].style.backgroundColor !== "") {
        continue;
    }
    td = tbody.children[i].children[0];

    id = td.children[td.childElementCount-1].href;
    id = id.split("/")[id.split("/").length-1];
    id = id.toLowerCase();

    name = td.children[td.childElementCount-1].title;

    data[data.length] = {id: id, name: name};
}
console.log(JSON.stringify(data));