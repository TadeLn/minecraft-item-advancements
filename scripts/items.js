data = [];
ul = [];

ul[0] = document.querySelector("#mw-content-text > div > div:nth-child(11) > ul");
ul[1] = document.querySelector("#mw-content-text > div > div:nth-child(13) > ul");
ul[2] = document.querySelector("#mw-content-text > div > div:nth-child(16) > ul");

for (var i = 0; i < ul.length; i++) {
    for (var j = 0; j < ul[i].childElementCount; j++) {
        console.log(i + " " + j);
        li = ul[i].children[j];
    
        id = li.children[li.childElementCount-1].href;
        id = id.split("/")[id.split("/").length-1];
        id = id.toLowerCase();
    
        name = li.children[li.childElementCount-1].title;
    
        data[data.length] = {id: id, name: name};
    }
}
console.log(JSON.stringify(data));