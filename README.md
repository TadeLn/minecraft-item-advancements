# Minecraft Item Checklist Generator

Minecraft Item Checklist Generator is a datapack generator that tracks every item you collect!

## How to use

1. Download the repository.
2. Install [python](https://www.python.org/) and add it to PATH.
3. Launch ``main.py``
4. It created ``itemChecklist`` folder. That is your datapack. Copy it into ``.minecraft/saves/your_world/datapacks/``

## How to modify

All item names and IDs are stored in ``data.json``. You can delete items you don't want on your list, rename them, or even add new items from mods! (NOT TESTED)

Short data.json example:
```json
[
    {"id": "minecraft:diamond", "name": "Diamond"},
    {"id": "minecraft:stone", "name": "Stone"},
    {"id": "minecraft:andesite", "name": "Andesite"},
    {"id": "minecraft:diorite", "name": "Diorite"},
    {"id": "minecraft:granite", "name": "Granite"},
    {"id": "minecraft:coal", "name": "Coal"},
    {"id": "minecraft:dirt", "name": "DIRT", "nbt": "{display:{Name:'{\"text\":\"DIRT\"}'}}"}
]
```

There are also other ``.json`` files in ``data`` folder. You can replace ``data.json`` with any of them.
- ``blocks.json`` - JSON exported out of a [Minecraft Wiki Page](https://minecraft.gamepedia.com/Blocks) using ``blocks.js`` script
- ``items.json`` - JSON exported out of a [Minecraft Wiki Page](https://minecraft.gamepedia.com/Items) using ``items.js`` script

If you want to modify some things like root advancement or letters, open ``main.py``, follow the comments and edit whatever you want.

## Additional scripts

These scripts are used to export blocks and their names to JSON format. They aren't perfect, some manual editing is still required.

- ``blocks.js`` - exports obtainable blocks from a [Minecraft Wiki Page](https://minecraft.gamepedia.com/Blocks) to a JSON format.
- ``items.js`` - exports items from a [Minecraft Wiki Page](https://minecraft.gamepedia.com/Items) to a JSON format.