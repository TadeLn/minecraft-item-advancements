import os
import json
import shutil

directory = "itemChecklist" # Datapack name / directory
namespace = "checklist" # Datapack's Namespace

pack_mcmeta = json.dumps({
    "pack": {
        "pack_format": 7,
        "description": "Item checklist created using TadeLn\'s Python script"
        }
    }, sort_keys=True, indent=4) # Contents of pack.mcmeta

show_toast = True               # show_toast option for item advancements
announce_to_chat = False        # announce_to_chat option for item advancements

letter_show_toast = True        # show_toast option for letter advancements
letter_announce_to_chat = False # announce_to_chat option for letter advancements

root_show_toast = True          # show_toast option for root advancements
root_announce_to_chat = False   # announce_to_chat option for root advancements

root_options = {
    "iconId": "minecraft:nether_star",
    "iconNbt": "",
    "background": "minecraft:textures/block/daylight_detector_side.png",
    "name": "Items",
    "description": "Collect all items in the game!"
} # Options for the root advancement

letter_options = {
    "name": "Items: %s",
    "description": "Collect all items starting with letter %s"
} # Options for the letter advancement (icons are in alphabetHeads below)


dir = directory + "/data/" + namespace + "/advancements/"

data = []
sortedData = {}

alphabet = "abcdefghijklmnopqrstuvwxyz"
alphabetHeads = (
    "{SkullOwner:{Id:\"e8e10bc5-b94e-4378-a54c-ac71a662fec9\",Name:\"a\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTY3ZDgxM2FlN2ZmZTViZTk1MWE0ZjQxZjJhYTYxOWE1ZTM4OTRlODVlYTVkNDk4NmY4NDk0OWM2M2Q3NjcyZSJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"d0f793ff-b041-427c-bc24-440834e986fa\",Name:\"b\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTBjMWI1ODRmMTM5ODdiNDY2MTM5Mjg1YjJmM2YyOGRmNjc4NzEyM2QwYjMyMjgzZDg3OTRlMzM3NGUyMyJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"db8ddc48-84d7-4f98-bce3-0fb2d43cd4dd\",Name:\"c\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWJlOTgzZWM0NzgwMjRlYzZmZDA0NmZjZGZhNDg0MjY3NjkzOTU1MWI0NzM1MDQ0N2M3N2MxM2FmMThlNmYifX19\"}]}}}",
    "{SkullOwner:{Id:\"c142dbd3-dbdf-4f46-a491-12162d3a3e8e\",Name:\"d\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzE5M2RjMGQ0YzVlODBmZjlhOGEwNWQyZmNmZTI2OTUzOWNiMzkyNzE5MGJhYzE5ZGEyZmNlNjFkNzEifX19\"}]}}}",
    "{SkullOwner:{Id:\"96eaca3a-0f22-484f-bad7-46153597e191\",Name:\"e\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGJiMjczN2VjYmY5MTBlZmUzYjI2N2RiN2Q0YjMyN2YzNjBhYmM3MzJjNzdiZDBlNGVmZjFkNTEwY2RlZiJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"5d098233-5c27-4e8a-a36f-959c9e35041b\",Name:\"f\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjE4M2JhYjUwYTMyMjQwMjQ4ODZmMjUyNTFkMjRiNmRiOTNkNzNjMjQzMjU1OWZmNDllNDU5YjRjZDZhIn19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"0b017eae-cd4d-48eb-9407-863e794c4bac\",Name:\"g\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWNhM2YzMjRiZWVlZmI2YTBlMmM1YjNjNDZhYmM5MWNhOTFjMTRlYmE0MTlmYTQ3NjhhYzMwMjNkYmI0YjIifX19\"}]}}}",
    "{SkullOwner:{Id:\"ba7a5f3c-db71-4827-8612-ce6bd82ee992\",Name:\"h\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzFmMzQ2MmE0NzM1NDlmMTQ2OWY4OTdmODRhOGQ0MTE5YmM3MWQ0YTVkODUyZTg1YzI2YjU4OGE1YzBjNzJmIn19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"2ab25358-deaa-46b7-b351-d9732a448ff1\",Name:\"i\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDYxNzhhZDUxZmQ1MmIxOWQwYTM4ODg3MTBiZDkyMDY4ZTkzMzI1MmFhYzZiMTNjNzZlN2U2ZWE1ZDMyMjYifX19\"}]}}}",
    "{SkullOwner:{Id:\"be1f1677-af64-4f59-85be-244f28eb1306\",Name:\"j\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2E3OWRiOTkyMzg2N2U2OWMxZGJmMTcxNTFlNmY0YWQ5MmNlNjgxYmNlZGQzOTc3ZWViYmM0NGMyMDZmNDkifX19\"}]}}}",
    "{SkullOwner:{Id:\"4f264d8d-87a3-496a-ad1d-eda0a22a30f0\",Name:\"k\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTQ2MWIzOGM4ZTQ1NzgyYWRhNTlkMTYxMzJhNDIyMmMxOTM3NzhlN2Q3MGM0NTQyYzk1MzYzNzZmMzdiZTQyIn19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"75ae0d27-f43c-416d-96a0-fdd6dbba7ab0\",Name:\"l\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzE5ZjUwYjQzMmQ4NjhhZTM1OGUxNmY2MmVjMjZmMzU0MzdhZWI5NDkyYmNlMTM1NmM5YWE2YmIxOWEzODYifX19\"}]}}}",
    "{SkullOwner:{Id:\"a291bc00-e8d7-42cf-8d57-588291f3ebc2\",Name:\"m\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDljNDVhMjRhYWFiZjQ5ZTIxN2MxNTQ4MzIwNDg0OGE3MzU4MmFiYTdmYWUxMGVlMmM1N2JkYjc2NDgyZiJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"439f4d78-dedd-427f-9973-c3906c4df529\",Name:\"n\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzViOGIzZDhjNzdkZmI4ZmJkMjQ5NWM4NDJlYWM5NGZmZmE2ZjU5M2JmMTVhMjU3NGQ4NTRkZmYzOTI4In19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"4ec45cf0-b927-444b-85fb-f5910bfae296\",Name:\"o\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDExZGUxY2FkYjJhZGU2MTE0OWU1ZGVkMWJkODg1ZWRmMGRmNjI1OTI1NWIzM2I1ODdhOTZmOTgzYjJhMSJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"6c22192b-5af9-4d07-a96f-f95b9e81d163\",Name:\"p\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTBhNzk4OWI1ZDZlNjIxYTEyMWVlZGFlNmY0NzZkMzUxOTNjOTdjMWE3Y2I4ZWNkNDM2MjJhNDg1ZGMyZTkxMiJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"047b3876-eac3-414b-8978-1c083fd8df86\",Name:\"q\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDM2MDlmMWZhZjgxZWQ0OWM1ODk0YWMxNGM5NGJhNTI5ODlmZGE0ZTFkMmE1MmZkOTQ1YTU1ZWQ3MTllZDQifX19\"}]}}}",
    "{SkullOwner:{Id:\"9af104ca-dfbb-4616-9101-d10016c2b11a\",Name:\"r\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTVjZWQ5OTMxYWNlMjNhZmMzNTEzNzEzNzliZjA1YzYzNWFkMTg2OTQzYmMxMzY0NzRlNGU1MTU2YzRjMzcifX19\"}]}}}",
    "{SkullOwner:{Id:\"826bfe2f-387c-44d7-9cd3-fc3046a6204d\",Name:\"s\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2U0MWM2MDU3MmM1MzNlOTNjYTQyMTIyODkyOWU1NGQ2Yzg1NjUyOTQ1OTI0OWMyNWMzMmJhMzNhMWIxNTE3In19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"08c08ff3-95f3-44cc-8da3-d0e1331cf469\",Name:\"t\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTU2MmU4YzFkNjZiMjFlNDU5YmU5YTI0ZTVjMDI3YTM0ZDI2OWJkY2U0ZmJlZTJmNzY3OGQyZDNlZTQ3MTgifX19\"}]}}}",
    "{SkullOwner:{Id:\"dd376034-312d-4515-9c70-eafbba1a12e4\",Name:\"u\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNjA3ZmJjMzM5ZmYyNDFhYzNkNjYxOWJjYjY4MjUzZGZjM2M5ODc4MmJhZjNmMWY0ZWZkYjk1NGY5YzI2In19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"18724f9a-8aab-4f3f-a0ff-26e4b704d53f\",Name:\"v\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2M5YTEzODYzOGZlZGI1MzRkNzk5Mjg4NzZiYWJhMjYxYzdhNjRiYTc5YzQyNGRjYmFmY2M5YmFjNzAxMGI4In19fQ==\"}]}}}",
    "{SkullOwner:{Id:\"20816a64-5743-4624-8d5e-00549a64179b\",Name:\"w\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjY5YWQxYTg4ZWQyYjA3NGUxMzAzYTEyOWY5NGU0YjcxMGNmM2U1YjRkOTk1MTYzNTY3ZjY4NzE5YzNkOTc5MiJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"33b5946c-20d1-466f-8dd5-11a0fdcd7105\",Name:\"x\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19\"}]}}}",
    "{SkullOwner:{Id:\"579ec4fb-0baf-4716-89ba-1c175fbd6e3c\",Name:\"y\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzUyZmIzODhlMzMyMTJhMjQ3OGI1ZTE1YTk2ZjI3YWNhNmM2MmFjNzE5ZTFlNWY4N2ExY2YwZGU3YjE1ZTkxOCJ9fX0=\"}]}}}",
    "{SkullOwner:{Id:\"0f3a3e42-4410-48a6-9d46-97392e7a6e27\",Name:\"z\",Properties:{textures:[{Value:\"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTA1ODJiOWI1ZDk3OTc0YjExNDYxZDYzZWNlZDg1ZjQzOGEzZWVmNWRjMzI3OWY5YzQ3ZTFlMzhlYTU0YWU4ZCJ9fX0=\"}]}}}"
)



def advancement_root(options):
   
    # Create an advancement
    advancement = {
        "display": {
            "icon": {
                "item": options["iconId"],
                "nbt": options["iconNbt"]
            },
            "background": options["background"],
            "title": options["name"],
            "frame": "challenge",
            "description": options["description"],
            "show_toast": root_show_toast,
            "announce_to_chat": root_announce_to_chat
        },
        "criteria": {}
    }

    # Check for empty nbt
    if options["iconNbt"] == "":
        advancement["display"]["icon"].pop("nbt")

    # Return advancement
    return advancement



def advancement_letter(letter, options):
    
    # Create an advancement
    advancement = {
        "display": {
            "icon": {
                "item": "minecraft:player_head",
                "nbt": alphabetHeads[alphabet.find(letter)]
            },
            "title": options["name"] % letter.upper(),
            "frame": "goal",
            "description": options["description"] % letter.upper(),
            "show_toast": letter_show_toast,
            "announce_to_chat": letter_announce_to_chat
        },
        "parent": namespace + ":root",
        "criteria": {}
    }

    return advancement



def advancement_end(letter, options):
    
    # Create an advancement
    advancement = {
        "display": {
            "icon": {
                "item": "minecraft:player_head",
                "nbt": alphabetHeads[alphabet.find(letter)]
            },
            "title": options["name"] % letter.upper(),
            "frame": "goal",
            "description": options["description"],
            "show_toast": False,
            "announce_to_chat": False
        },
        "parent": options["previousAdvancement"],
        "criteria": {
            "tick": {
                "trigger": "minecraft:tick"
            }
        }
    }

    return advancement



def itemAdvancement(item, previousAdvancement):

    # Create an advancement
    advancement = {
        "display": {
            "icon": {
                "item": item["id"],
                "nbt": item["nbt"]
            },
            "title": item["name"],
            "frame": "task",
            "description": "Collect " + item["name"],
            "show_toast": show_toast,
            "announce_to_chat": announce_to_chat
        },
        "parent": previousAdvancement,
        "criteria": {
            "collect": {
                "trigger": "minecraft:inventory_changed",
                "conditions": {
                    "items": [
                        {
                            "items": [item["id"]],
                            "nbt": item["nbt"],
                        }
                    ]
                }
            }
        }
    }

    # Check for empty nbt
    if item["nbt"] == "":
        advancement["display"]["icon"].pop("nbt")
        advancement["criteria"]["collect"]["conditions"]["items"][0].pop("nbt")

    # Return advancement
    return advancement



def itemAdvancement_init():
    global data

    # Create a directory
    try:  
        os.makedirs("./" + dir) 
        print("Created a new directory: \"./" + dir + "\"")
    except FileExistsError:
        pass

    #################### ROOT ####################

    # Create a root advancement
    print("Creating root advancement...")
    rootadvancement = advancement_root(root_options)

    # Create criteria for the root advancement
    print("  Creating criteria for root...")
    for x in data:
        print("    Creating criteria for root: " + x["id"] + "...")
        rootadvancement["criteria"][x["id"] + "_" + x["nbt"]] = {
            "trigger": "minecraft:inventory_changed",
            "conditions": {
                "items": [
                    {
                        "items": [x["id"]],
                        "nbt": x["nbt"]
                    }
                ]
            }
        }

        # Check for empty nbt
        if x["nbt"] == "":
            rootadvancement["criteria"][x["id"] + "_" + x["nbt"]]["conditions"]["items"][0].pop("nbt")
    print("  Creating criteria for root... Done")

    # Write to root.json
    file = open(dir + "root.json", "w+")
    file.write(json.dumps( rootadvancement, sort_keys=True, indent=4 ))
    file.close()
    print("Creating root advancement... Done")

    #################### LETTERS ####################

    # Loop through all letters
    for letter in alphabet:

        # Create a letter advancement
        print("Creating letter advancements: " + letter + "...")
        letteradvancement = advancement_letter(letter, letter_options)

        # Create criteria for the letter advancement
        print("  Creating criteria for letter " + letter + "...")
        for x in sortedData[letter]:
            print("    Creating criteria for letter " + letter + ": " + x["id"])
            letteradvancement["criteria"][x["id"] + "_" + x["nbt"]] = {
                "trigger": "minecraft:inventory_changed",
                "conditions": {
                    "items": [
                        {
                            "items": [x["id"]],
                            "nbt": x["nbt"]
                        }
                    ]
                }
            }

            # Check for empty nbt
            if x["nbt"] == "":
                letteradvancement["criteria"][x["id"] + "_" + x["nbt"]]["conditions"]["items"][0].pop("nbt")
        print("  Creating criteria for letter " + letter + "... Done")

        # Write to letter.json
        file = open(dir + letter + ".json", "w+")
        file.write(json.dumps( letteradvancement, sort_keys=True, indent=4 ))
        file.close()
        print("Creating letter advancements: " + letter + "... Done")
    
    #################### ITEMS ####################

    # Loop through the alphabet
    print("Creating item advancements...")
    filenames = []
    for letter in alphabet:
        previousAdvancement = namespace + ":" + letter

        # Loop through all in sortedData
        for x in sortedData[letter]:
            print("  Creating item advancements: " + x["id"])
            
            fn = x["id"].replace(":", "_") # filename
            sdir = "" # subdirectory

            if fn in filenames:
                i = 0
                while fn + "/%d" % i in filenames:
                    i = i + 1
                sdir = fn + "/"
                fn = "%d" % i

            filenames.append(sdir + fn)

            # Create a directory
            try:  
                os.makedirs("./" + dir + sdir) 
                print("Created a new directory: \"./" + dir + sdir + "\"")
            except FileExistsError:
                pass
            
            # Write to a file
            file = open(dir + sdir + fn + ".json", "w+")
            file.write(json.dumps( itemAdvancement(x, previousAdvancement), sort_keys=True, indent=4 ))
            file.close()

            previousAdvancement = namespace + ":" + sdir + fn
        
        print(previousAdvancement)
        file = open(dir + "_" + letter + ".json", "w+")
        file.write(json.dumps( advancement_end(letter, {"name": "%s", "description": "", "previousAdvancement" : previousAdvancement}), sort_keys=True, indent=4) )
        file.close()
        
    print("Creating item advancements... Done")



def init():
    global data
    global sortedData

    # Load a file into data
    file = open("data.json", "r")
    if file.mode == 'r':
        print("Reading file data.json...")
        fileContent = file.read()
    else:
        # If unable to open a file...
        print("Unable to open file data.json")
        return
    
    data = json.loads(fileContent)
    file.close()
    print("Reading file data.json... Done")

    # Replace the lack of nbt with empty strings
    for x in data:
        if "nbt" not in x:
            x["nbt"] = ""

    # Sort data
    sortedData = {}
    print("Sorting data...")
    for letter in alphabet:
        print("Sorting data: " + letter)
        sortedData[letter] = []
        for x in data:
            if x["name"].lower()[0] == letter:
                sortedData[letter].append(x)
    print("Sorting data... Done")
    
    # Remove old datapack
    if os.path.exists(directory):
        if not os.path.isfile(directory):
            print("Old datapack found.")
            print("Removing old datapack...")
            shutil.rmtree(directory)
            print("Removing old datapack... Done")
        else:
            print("That should never happen but ok. I fixed this.")
            os.remove(directory)
    else:
        print("No old datapack found.")

    itemAdvancement_init()

    # Create pack.mcmeta file
    print("Creating pack.mcmeta file...")
    file = open(directory + "/pack.mcmeta", "w+")
    file.write(pack_mcmeta)
    file.close()
    print("Creating pack.mcmeta file... Done")
    print("The datapack is ready!")

init()
